#!/usr/bin/env bash

x86_pkgbuilds=$(find ../arctic-pkgbuilds -type f -name "*.pkg.tar.zst*")

for x in $x86_pkgbuilds
do
    mv "$x" x86_64/
    echo "Moving $x"
done

echo "##########################"
echo "Building the repo database"
echo "##########################"

cd x86_64
rm -r arctic-core-repo*

repo-add -n -R arctic-core-repo.db.tar.gz *.pkg.tar.zst

rm arctic-core-repo.db
rm arctic-core-repo.files
mv arctic-core-repo.db.tar.gz arctic-core-repo.db
mv arctic-core-repo.files.tar.gz arctic-core-repo.files

echo "#############################################"
echo "Repo packages have been successfully updated!"
echo "#############################################"

git add .
git commit -m "Update database"
git push origin master

echo "#####################"
echo "Repo pushed to Gitlab"
echo "#####################"