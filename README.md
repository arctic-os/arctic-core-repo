# arctic-core-repo

While this repository list is primarily developed for Arctic OS, you can install it on any arch-based distro by appending the following code to the end of `/etc/pacman.conf`.

```
[arctic-core-repo]
SigLevel = Optional TrustAll
Server = https://gitlab.com/arctic-os/$repo/-/raw/master/$arch
```
